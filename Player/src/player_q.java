
public class player_q {
	 // 큐 배열은 head와 tail 그리고 maxSize를 가진다.
    private int head;
    private int tail;
    private int maxSize;
    private Object[] queueArray;
    
    // 큐 배열 생성
    public player_q(int maxSize){
        
        this.head = 0;
        this.tail = -1;
        this.maxSize = maxSize;
        this.queueArray = new Object[maxSize];
    }
    
    // 큐가 비어있는지 확인
    public boolean empty(){
        return (head == tail+1);
    }
    
    // 큐가 꽉 찼는지 확인
    public boolean full(){
        return (tail == maxSize-1);
    }
    
    // 큐 tail에 데이터 등록
    public void insert(Object item){
        
        if(full()) throw new ArrayIndexOutOfBoundsException();
        
        queueArray[++tail] = item;
    }
    
    // 큐에서 head 데이터 조회
    public Object peek(){
        
        if(empty()) throw new ArrayIndexOutOfBoundsException();
        
        return queueArray[head];
    }
    
    // 큐에서 head 데이터 제거
    public Object remove(){
        
        Object item = peek();
        head++;
        return item;
    }
}
