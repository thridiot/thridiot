package thridiot;

public class HexagonMap 
{
	//(0, 0)노드
	public Node core;
	
	private int orbit;
	
	//방향을 나타냄, 처음 방향 : 1
	private Direction direction = new Direction(); 
	
	//core를초기화하고 본격적인 맵생성시작
	public HexagonMap(int numberOfOrbits)
	{
		core = new Node(0,0);
		
		core.setX(440);
		
		core.setY(315);
		
		orbit = numberOfOrbits;
		
		makeMap(numberOfOrbits);
	}
	
	public int getOrbit()
	{
		return orbit;
	}
	
	//int값 하나를 입력받고 그만큼의 궤도를 가진 맵 생성 
	protected void makeMap(int input)
	{	
		//지금 작업중인 노드를 잠깐 넣어놓는 곳
		Node tempNode;
		
		//궤도반복, 총 input - 1번 반복
		for(int i = 1; i < input; i++)
		{
			//궤도의 첫 노드 생성
			tempNode = new Node(i, 0);
			
			//궤도의 첫 노드의 좌표는 
			tempNode.setX(this.getNode(i - 1, 0).getX() + this.getNode(i - 1, 0).getWidth());
			
			tempNode.setY(this.getNode(i - 1, 0).getY());
			
			//그 전 궤도의 첫노드와 연결
			tempNode.setLinkedNode(5, this.getNode(i - 1, 0));
			
			//방향값을 1로 리셋
			direction.reset();
			
			//순서반복, 총 (6 * i) - 1번 반복
			for(int j = 1; j < (6 * i); j++)
			{
				//궤도의 j번째 노드 생성
				tempNode = new Node(i, j);
				
				//진행방향을 얻어와서 자신의 전 노드와 연결, 방향은 Node클래스 주석 참조
				tempNode.setLinkedNode(direction.getDir(0), this.getNode(i, j - 1));
				
				tempNode.setLocation(direction.getDir(0), this.getNode(i, j - 1));
				
				//전 노드를 자신의 previousNode에 넣음, 서치에 쓰임
				tempNode.setLinkedNode(7, this.getNode(i, j - 1));
				
				//진행방향을 얻어와서 자신의 이전 궤도의 인접 노드와 연결, 방향은 Node클래스 주석 참조
				tempNode.setLinkedNode(direction.getDir(- 1), this.getNode(i - 1, j - direction.getDirNum(0)));
				
				//노드가 맵의 꼭지점에 있다면
				if(j % i == 0)
				{
					//방향을 1높힘
					direction.increase();
				}
				
				//노드가 꼭지점에 있지 않다면
				else
				{
					//진행방향을 얻어와서 자신의 이전 궤도의 인접 노드와 연결(최근에 연결한 노드의 다음 노드), 방향은 Node클래스 주석 참조
					tempNode.setLinkedNode((direction.getDir( - 2)), this.getNode(i - 1, j - direction.getDirNum(- 1)));
				}
				
				//연결이 완료되면 콘솔에 노드를 표시
				System.out.println("(" + tempNode.getOrbit() + ", " + tempNode.getOrder() + ")");
				
				System.out.println("(" + tempNode.getX() + ", " + tempNode.getY() + ")");
				
			}
			
			//궤도의 마지막 노드는 궤도의 첫 노드와 60-240노드
			tempNode.setLinkedNode(3, this.getNode(i, 0));
			
			//궤도의 마지막 노드는 궤도의 다음 노드를 첫 노드로 만듬
			tempNode.setLinkedNode(8, this.getNode(i, 0));
		}
	}
	
	//궤도와 순서를 받아서 맵을 서치
	public Node getNode(int inputOrbit, int inputOrder)
	{
		//core에서 시작
		Node tempNode = this.core;
		
		//입력된 궤도에 도달할 때까지 0방향 노드로 이동
		for(int i = 0; i < inputOrbit; i++)
		{
			tempNode = tempNode.getLinkedNode(2);
		}
		
		//입력된 순서까지 다음 노드로 이동
		for(int i = 0; i < inputOrder; i++)
		{
			tempNode = tempNode.getLinkedNode(8);
		}
		
		return tempNode;
	}
	
	public boolean isCorner(int i, int j)
	{
		if((i == 1 && j == 1) || (i == 1 && j == 2) || (i == 1 && j == 3) || (i == 1 && j == 4) || (i == 1 && j == 5) ||
		   (i == 2 && j == 2) || (i == 2 && j == 4) || (i == 2 && j == 6) || (i == 2 && j == 8) || (i == 2 && j == 10) ||
		   (i == 3 && j == 3) || (i == 3 && j == 6) || (i == 3 && j == 9) || (i == 3 && j == 12) || (i == 3 && j == 15) ||
		   (i == 4 && j == 4) || (i == 4 && j == 8) || (i == 4 && j == 12) || (i == 4 && j == 16) || (i == 4 && j == 20) ||
		   (i == 5 && j == 5) || (i == 5 && j == 10) || (i == 5 && j == 15) || (i == 5 && j == 20) || (i == 5 && j == 25) ||
		   (i == 6 && j == 6) || (i == 6 && j == 12) || (i == 6 && j == 18) || (i == 6 && j == 24) || (i == 6 && j == 30) ||
		   (i == 7 && j == 7) || (i == 7 && j == 14) || (i == 7 && j == 21) || (i == 7 && j == 28) || (i == 7 && j == 35) ||
		   (i == 8 && j == 8) || (i == 8 && j == 16) || (i == 8 && j == 24) || (i == 8 && j == 32) || (i == 8 && j == 40))
			
		{
			return true;
		}
		
		else
		{
			return false;
		}
		
	}
	
}
