package thridiot;

import java.awt.Graphics;

import java.awt.Font;

import java.awt.Graphics;

import javax.swing.ImageIcon;

import javax.swing.JButton;

import javax.swing.JLabel;

import javax.swing.JPanel;

public class Control
{
	private Node center;
	
	int centerX;
	
	int centerY;
	
	public CenterPanel turnPicture;
	public JButton button0;
	public JButton button60;
	public JButton button120;
	public JButton button180;
	public JButton button240;
	public JButton button300;
	public JLabel turnpassL;
	
	public void setCenter(Player input, TurnManager turnManager, GameManager gameManager)
	{
		center = input.getHead();
		
		turnPicture.tile = center.tile;
		
		if(center.getLinkedNode(1) == null)
		{
			button300.setEnabled(false);
		}
		
		else if(center.getLinkedNode(1).getData() != 0)
		{
			button300.setEnabled(false);
		}
		
		else
		{
			button300.setEnabled(true);
			
		}
		
		if(center.getLinkedNode(2) == null)
		{
			button0.setEnabled(false);
		}
		
		else if(center.getLinkedNode(2).getData() != 0)
		{
			button0.setEnabled(false);
		}
		
		else
		{
			button0.setEnabled(true);
		}
		
		if(center.getLinkedNode(3) == null)
		{
			button60.setEnabled(false);
		}
		
		else if(center.getLinkedNode(3).getData() != 0)
		{
			button60.setEnabled(false);
		}
		
		else
		{
			button60.setEnabled(true);
		}
		
		if(center.getLinkedNode(4) == null)
		{
			button120.setEnabled(false);
		}
		
		else if(center.getLinkedNode(4).getData() != 0)
		{
			button120.setEnabled(false);
		}
		
		else
		{
			button120.setEnabled(true);
		}
		
		if(center.getLinkedNode(5) == null)
		{
			button180.setEnabled(false);
		}
		
		else if(center.getLinkedNode(5).getData() != 0)
		{
			button180.setEnabled(false);
		}
		
		else
		{
			button180.setEnabled(true);
		}
		
		if(center.getLinkedNode(6) == null)
		{
			button240.setEnabled(false);
		}
		
		else if(center.getLinkedNode(6).getData() != 0)
		{
			button240.setEnabled(false);
		}
		
		else
		{
			button240.setEnabled(true);
		}
	}
	
	public Control(Node first)
	{
		center = first;
		
		centerX = 1000;
				
		centerY = 315;
		
		turnPicture = new CenterPanel();
		button0 = new JButton();
		button60 = new JButton();
		button120 = new JButton();
		button180 = new JButton();
		button240 = new JButton();
		button300 = new JButton();
		
		ImageIcon control0 = new ImageIcon("control0.png");
		ImageIcon control60 = new ImageIcon("control60.png");
		ImageIcon control120 = new ImageIcon("control120.png");
		ImageIcon control180 = new ImageIcon("control180.png");
		ImageIcon control240 = new ImageIcon("control240.png");
		ImageIcon control300 = new ImageIcon("control300.png");
		
		button0.setText("");
		button0.setActionCommand("0");
		button0.setIcon(control0);
		button60.setText("");
		button60.setActionCommand("60");
		button60.setIcon(control60);
		button120.setText("");
		button120.setActionCommand("120");
		button120.setIcon(control120);
		button180.setText("");
		button180.setActionCommand("180");
		button180.setIcon(control180);
		button240.setText("");
		button240.setActionCommand("240");
		button240.setIcon(control240);
		button300.setText("");
		button300.setActionCommand("300");
		button300.setIcon(control300);
		
		turnPicture.setBounds(centerX, centerY, 56, 56);
		button0.setBounds(centerX + 56, centerY, 56, 56);
		button60.setBounds(centerX + 28, centerY - 56, 56, 56);
		button120.setBounds(centerX - 28, centerY - 56, 56, 56);
		button180.setBounds(centerX - 56, centerY, 56, 56);
		button240.setBounds(centerX - 28, centerY + 56, 56, 56);
		button300.setBounds(centerX + 28, centerY + 56, 56, 56);
		
		turnpassL = new JLabel("TURN PASS : 3");
		turnpassL.setFont(new Font("Monospaced", Font.PLAIN, 25));
		turnpassL.setBounds(centerX - 56, centerY + 150, 200,80);
		
		
	}
	
}

