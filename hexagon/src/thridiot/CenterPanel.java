package thridiot;

import java.awt.Graphics;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

public class CenterPanel extends JPanel
{
	ImageIcon tile;
	
	public void paintComponent(Graphics g) 
	{
		g.drawImage(tile.getImage(), 0, 0, null);
		
		setOpaque(false);
		
		super.paintComponent(g);
	}
}
