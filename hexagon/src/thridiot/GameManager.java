package thridiot;

import java.util.*;

import java.util.ArrayList;

public class GameManager 
{
	HexagonMap map = new HexagonMap(8);
	
	Player player1 = new Player(1);
	
	Player player2 = new Player(2);
	
	Player player3 = new Player(3);
	
	ArrayList<Node> barricade = new ArrayList<Node>();

	public HexagonMap getMap()
	{
		return this.map;
	}
	
	//게임시작 처음상테
	public void initiatePlayer()
	{
		map.getNode(7, 21).setStatus(5);
		
		player1.move(map.getNode(7, 21));
		
		player1.setEndNode(map.getNode(7, 0));
		
		map.getNode(7, 14).setStatus(6);
		
		player2.move(map.getNode(7, 14));
		
		player2.setEndNode(map.getNode(7, 35));
		
		map.getNode(7, 7).setStatus(7);
		
		player3.move(map.getNode(7, 7));
		
		player3.setEndNode(map.getNode(7, 28));
	}
	
	public Player getPlayer (int input)
	{
		if(input == 1)
		{
			return player1;
		}
		
		else if(input == 2)
		{
			return player2;
		}
		
		else if(input == 3)
		{
			return player3;
		}
		
		else
		{
			return player3;
		}
		
	}
	
	public void loseCheck()
	{
		if(!player1.isAlive())
		{
			while(player1.getPiece().isEmpty())
			{
				barricade.add(player1.getPiece().remove());
			}
		}
		
		if(!player2.isAlive())
		{
			while(player2.getPiece().isEmpty())
			{
				barricade.add(player2.getPiece().remove());
			}
		}
		
		if(!player3.isAlive())
		{
			while(player3.getPiece().isEmpty())
			{
				barricade.add(player3.getPiece().remove());
			}
		}
		
	}
	
	public void thridiot()
	{
		if(player1.getAvailablePass() == 0 && player2.getAvailablePass() == 0 && player3.getAvailablePass() == 0  )
		{
			
		}
	}

	public void movePiece(Player player, Node newHead)
	{
		if(player.getPiece().getTail() == 4)
		{
			player.getPiece().peek().setStatus(0);
		}
		
		player.getHead().setStatus(player.getPlayerNumber());
		
		newHead.setStatus(player.getPlayerNumber() + 4);
		
		player.move(newHead);
	}

}

