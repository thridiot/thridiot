package thridiot;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Random;
import javax.imageio.ImageIO;
import javax.swing.*;

public class TFrame extends JFrame 
{
	GameManager gameManager = new GameManager();
	
	TurnManager turnManager = new TurnManager();
	
	Control control;
	
	Container c;
	
	Ending ending = new Ending();
	
	Random random = new Random();

	Image background = Toolkit.getDefaultToolkit().getImage("background.png");
	JPanel SPanel = new JPanel(){
		   public void paintComponent(Graphics g){
			    //super.paintComponents(g);
			    g.drawImage(background, 0, 0, 1280, 720, this);
			   }
			  }; // 시작 패널
	
	JPanel RPanel = new JPanel(); // 룰 설명 패널
	
	JPanel PPanel = new JPanel(); // 게임플레이 패널
	
	JLabel ruleL = new JLabel();
	ImageIcon ruleP1 = new ImageIcon("rule1.png");
	ImageIcon ruleP2 = new ImageIcon("rule2.png");
	ImageIcon ruleP3 = new ImageIcon("rule3.png");
	ImageIcon ruleP4 = new ImageIcon("rule4.png");
	
	public CardLayout card = new CardLayout(0,0);

	int numberOfBarricade;
	
	int turn = turnManager.next();
	
	private void drawMap()
	{
		Node tempNode = gameManager.getMap().getNode(0, 0);
		
		tempNode.setLocation(getX(), getY());
		
		PPanel.add(tempNode);
		
		for(int i = 1; i < gameManager.getMap().getOrbit(); i++)
		{	
			for(int j = 0; j < (6 * i); j++)
			{
				tempNode = gameManager.getMap().getNode(i, j);
				
				tempNode.setLocation(getX(), getY());
				
				PPanel.add(tempNode);
			}
		}
		
		for(int i = 0; i <= 41; i++)
		{
			gameManager.getMap().getNode(7, i).setStatus(4);
		}
		
		gameManager.getMap().getNode(7, 28).setStatus(10);
		
		gameManager.getMap().getNode(7, 35).setStatus(9);
		
		gameManager.getMap().getNode(7, 0).setStatus(8);

	}

	private void makeControlPanel()
	{
		
		control = new Control(gameManager.getPlayer(turn).getHead());
		
		control.button0.addActionListener(new ControlActionListener());
		control.button60.addActionListener(new ControlActionListener());
		control.button120.addActionListener(new ControlActionListener());
		control.button180.addActionListener(new ControlActionListener());
		control.button240.addActionListener(new ControlActionListener());
		control.button300.addActionListener(new ControlActionListener());
		
		PPanel.add(control.turnPicture);
		PPanel.add(control.button0);
		PPanel.add(control.button60);
		PPanel.add(control.button120);
		PPanel.add(control.button180);
		PPanel.add(control.button240);
		PPanel.add(control.button300);
		
		control.setCenter(gameManager.getPlayer(turn), turnManager, gameManager);

		PPanel.add(control.turnpassL);
		
		control.setCenter(gameManager.getPlayer(turn), turnManager, gameManager);

		repaint();

	}
	
	private void randomBarricade()
	{
		int tempX;
		
		int tempY;
		
		for(int i = 0; i < numberOfBarricade; i++)
		{
			System.out.println(i);
			
			tempX = random.nextInt(6) + 1;
			
			tempY = random.nextInt(6 * tempX - 1);
			
			gameManager.getMap().getNode(tempX, tempY).setStatus(4);
		}
	}
	
	private void victoryCheck()
	{
		if(gameManager.player1.victory())
		{
			ending.ending(gameManager.player1, gameManager);
		}
		
		else if(gameManager.player2.victory())
		{
			ending.ending(gameManager.player2, gameManager);
		}
		else if(gameManager.player3.victory())
		{
			ending.ending(gameManager.player2, gameManager);
		}
		
	}
	
	TFrame() 
	{
		int temp1 = 60 / 2;
		
		int temp2 = 60 * 3 / 4;
		
		setTitle("THRIDIOT"); // 프레임 생성
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // 프레임 윈도우를 닫으면 프로그램 종료
		
		c = getContentPane();
		c.setLayout(card);
		
		//************************************************
		
		SPanel.setLayout(null);
		
		//시작버튼
		JButton startB = new JButton("START");
		startB.setFont(new Font("Monospaced", Font.PLAIN, 30));
		startB.setBounds(560, 430, 140, 60);
		startB.setContentAreaFilled(false);
		startB.addActionListener(new MyActionListener());
		SPanel.add(startB);
		
		//룰설명 버튼
		JButton ruleB = new JButton("RULE");
		ruleB.setFont(new Font("Monospaced", Font.PLAIN, 30));
		ruleB.setBounds(560, 500, 140, 60);
		ruleB.setContentAreaFilled(false);
		ruleB.addActionListener(new MyActionListener());
		SPanel.add(ruleB);
		
		//************************************************
		
		RPanel.setBackground(Color.WHITE);
		
		//룰 설명용 그림을 삽입
		ruleL.setIcon(ruleP1);
		RPanel.add(ruleL, BorderLayout.CENTER);
		
		JButton returnB = new JButton("RETURN");
		returnB.setFont(new Font("Monospaced", Font.PLAIN, 30));
		returnB.setAlignmentX(Component.CENTER_ALIGNMENT);
		returnB.setContentAreaFilled(false);
		returnB.addActionListener(new MyActionListener());
		RPanel.add(returnB,BorderLayout.SOUTH);
		
		JButton nextB = new JButton("NEXT");
		nextB.setFont(new Font("Monospaced", Font.PLAIN, 30));
		nextB.setAlignmentX(Component.CENTER_ALIGNMENT);
		nextB.setContentAreaFilled(false);
		nextB.addActionListener(new MyActionListener());
		RPanel.add(nextB,BorderLayout.SOUTH);
		
		//************************************************
		
		PPanel.setBackground(Color.WHITE);
		PPanel.setLayout(null);
			
		drawMap();
		
		gameManager.initiatePlayer();
		
		makeControlPanel();
		
		
		
		//************************************************
		
		c.add("SPanel", SPanel);
		c.add("RPanel", RPanel);
		c.add("PPanel", PPanel);
		
		card.show(c,"SPanel");
		
		setResizable(false);
		setSize(1280, 720); // 프레임 크기
		setVisible(true); // 화면에 프레임 출력
		
	}
	
	class MyActionListener implements ActionListener 
	{
    
	    public void actionPerformed(ActionEvent e) // 버튼이 클릭될 때 호출되는 메소드
	    {
	        JButton b = (JButton)e.getSource(); // 이벤트 소스 버튼 알아내기 / 사용자가 클릭한 버튼 알아내기
	        Icon nowP = ruleL.getIcon();
	        
	        if(b.getText().equals("START")) 
	        {
	        	String[] selections = {"1", "2", "3", "4", "5", "6"};
	        	
	        	numberOfBarricade = Integer.parseInt((String) JOptionPane.showInputDialog(null, "몇개의 장애물을 만들까요?", "몇개?", JOptionPane.QUESTION_MESSAGE, null, selections, "1"));
	        	randomBarricade();
	        	card.show(c, "PPanel");
	        }
	        
	        else if(b.getText().equals("RULE")) // 룰설명 화면으로 전환
	        {
	        	card.show(c, "RPanel");
	        }
	        
	        else if(b.getText().equals("RETURN")) // 시작화면으로 돌아감
	        {
	        	if(nowP.equals(ruleP1))
	        		card.show(c, "SPanel");
	        	else if(nowP.equals(ruleP2))
	        		ruleL.setIcon(ruleP1);
	        	else if(nowP.equals(ruleP3))
	        		ruleL.setIcon(ruleP2);
	        	else if(nowP.equals(ruleP4))
	        		ruleL.setIcon(ruleP3);
	        }
	        
	        else if(b.getText().equals("NEXT")) // 다음 설명문을 보여줌
	        {
	        	if(nowP.equals(ruleP1))
	        		ruleL.setIcon(ruleP2);
	        	else if(nowP.equals(ruleP2))
	        		ruleL.setIcon(ruleP3);
	        	else if(nowP.equals(ruleP3))
	        		ruleL.setIcon(ruleP4);
	        	else if(nowP.equals(ruleP4)){
	        		card.show(c, "SPanel");
	        		ruleL.setIcon(ruleP1);
	        	}
	        }
	      
	    }
	    
	}

	class ControlActionListener implements ActionListener 
	{
		
	    public void actionPerformed(ActionEvent e)
	    {
	        String b2 = e.getActionCommand();
	        
	        if(b2.equals("0"))
	        {
	        	gameManager.movePiece(gameManager.getPlayer(turn), gameManager.getPlayer(turn).getHead().getLinkedNode(2));
	        	
	        	turn = turnManager.next();
	        	
	        	control.setCenter(gameManager.getPlayer(turn), turnManager, gameManager);
	        
	        	victoryCheck();
	        	
	        	repaint();
	        }
	        
	        else if(b2.equals("60"))
	        {
	        	gameManager.movePiece(gameManager.getPlayer(turn), gameManager.getPlayer(turn).getHead().getLinkedNode(3));
	        	
	        	turn = turnManager.next();
	        	
	        	control.setCenter(gameManager.getPlayer(turn), turnManager, gameManager);
	        	
	        	victoryCheck();
	        	
	        	repaint();
	        }
	        
	        else if(b2.equals("120"))
	        {
	        	gameManager.movePiece(gameManager.getPlayer(turn), gameManager.getPlayer(turn).getHead().getLinkedNode(4));
	        	
	        	turn = turnManager.next();
	        	
	        	control.setCenter(gameManager.getPlayer(turn), turnManager, gameManager);
	        		      
	        	victoryCheck();
	        	
	        	repaint();
	        }
	        
	        else if(b2.equals("180"))
	        {
	        	gameManager.movePiece(gameManager.getPlayer(turn), gameManager.getPlayer(turn).getHead().getLinkedNode(5));
	        	
	        	turn = turnManager.next();
	        	
	        	control.setCenter(gameManager.getPlayer(turn), turnManager, gameManager);
	     	        
	        	victoryCheck();
	        	
	        	repaint();
	        }
	        
	        else if(b2.equals("240"))
	        {
	        	gameManager.movePiece(gameManager.getPlayer(turn), gameManager.getPlayer(turn).getHead().getLinkedNode(6));
	        	
	        	turn = turnManager.next();
	        	
	        	control.setCenter(gameManager.getPlayer(turn), turnManager, gameManager);
	        
	        	victoryCheck();
	        	
	        	repaint();
	        }
	        
	        else if(b2.equals("300"))
	        {
	        	gameManager.movePiece(gameManager.getPlayer(turn), gameManager.getPlayer(turn).getHead().getLinkedNode(1));
	        	
	        	turn = turnManager.next();
	        	
	        	control.setCenter(gameManager.getPlayer(turn), turnManager, gameManager);
	        	
	        	victoryCheck();
	        	
	        	repaint();
	        }
	        
	        //control.turnpassL.setText("TURN PASS : " + turnpass); // 턴 패스 갱신
	        
	    }
	    
	}
	
	public static void main(String [] args) 
	{
		
		new TFrame();
		
	}
	
}