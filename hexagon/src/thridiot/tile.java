package thridiot;

import java.awt.Graphics;

import javax.swing.ImageIcon;

import javax.swing.JPanel;

public class tile extends JPanel
{
	ImageIcon tile = new ImageIcon("tile1.png"); 
	
	public int getHeight()
	{
		return tile.getIconHeight();
	}
	
	public int getWidth()
	{
		return tile.getIconWidth();
	}
	
	public void paintComponent(Graphics g) 
	{
		g.drawImage(tile.getImage(), 0, 0, null);
		
		setOpaque(false);
		
		super.paintComponent(g);
	}
}
