package thridiot;

//방향을 표시하는 클래스
public class Direction 
{
	
	/*
	 숫자(방향값)	진행방향
	 1			120
	 2			180
	 3			240
	 4			300
	 5			0
	 6			60
	*/

	//방향값
	private int dir;
	
	int temp;
	
	public Direction()
	{
		dir = 1;
	}
	
	//int값 하나를 받고 그 값과 지금 방향값을 계산하여 리턴
	
	public int getDirNum(int variation)
	{
		if(dir == 1 && variation == 1)
		{
			return 0;
		}
		
		//방향값 + 받은 값이 6보다크면 1리턴
		if(dir + variation > 6)
		{
			temp = 1;
		}
		
		//방향값 + 받은 값이 1보다작으면 6리턴
		else if(dir + variation < 1)
		{
			temp = 0;
		}
		
		//아니라면 현재 방향값을 리턴
		else
		{
			temp = (dir + variation);
		}
		
		return temp;
	}
	
		
		public int getDir(int variation)
		{
			if(dir == 1)
			{
				
				if(variation == 0)
				{
					return 1;
				}
				
				else if(variation == -1)
				{
					return 6;
				}
				
				else if(variation == -2)
				{
					return 5;
				}
			}
			
			else if(dir == 2)
			{
				if(variation == 0)
				{
					return 2;
				}
				
				else if(variation == -1)
				{
					return 1;
				}
				
				else if(variation == -2)
				{
					return 6;
				}
			}
			
			else if(dir == 3)
			{
				if(variation == 0)
				{
					return 3;
				}
				
				else if(variation == -1)
				{
					return 2;
				}
				
				else if(variation == -2)
				{
					return 1;
				}
			}
			
			else if(dir == 4)
			{
				if(variation == 0)
				{
					return 4;
				}
				
				else if(variation == -1)
				{
					return 3;
				}
				
				else if(variation == -2)
				{
					return 2;
				}
			}
			
			else if(dir == 5)
			{
				if(variation == 0)
				{
					return 5;
				}
				
				else if(variation == -1)
				{
					return 4;
				}
				
				else if(variation == -2)
				{
					return 3;
				}
			}
			else if(dir == 6)
			{
				if(variation == 0)
				{
					return 6;
				}
				
				else if(variation == -1)
				{
					return 5;
				}
				
				else if(variation == -2)
				{
					return 4;
				}
			}
			
			return dir;
		}
	
	//방향값을 1높히고 만약 6이 넘으면 1로 바꿈
	public void increase()
	{
		
		dir++;
		
		if(dir > 6)
		{
			dir = 1;
		}
		
	}
	
	//방향값을 1낮추고 만약 1보다 작으면 6으로 바꿈
	public void decrease()
	{
		dir--;
		
		if(dir < 1)
		{
			dir = 6;
		}
		
	}
	
	//방향값을 1로
	public void reset()
	{
		dir = 1;
	}
	
}
