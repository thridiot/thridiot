package thridiot;

import java.util.Random;

public class TurnManager
{
    private int data;
    
    int player[];
    
    public TurnManager()
    {
    	Random random = new Random();
    	
    	data = 0;
    	
    	player = new int[3];
    	
    	int temp = random.nextInt(6);
    	
    	switch(temp)
    	{
    		case 0:
    		
    			player[0] = 1;
    			player[1] = 2;
    			player[2] = 3;
    		
    			break;
    		
    		case 1:
    		
    			player[0] = 1;
    			player[1] = 3;
    			player[2] = 2;
    		
    			break;
    	
    		case 2:
    		
    			player[0] = 2;
    			player[1] = 1;
    			player[2] = 3;
    		
    			break;
    		
    		case 3:
    		
    			player[0] = 2;
    			player[1] = 3;
    			player[2] = 1;
    		
    			break;
    		
    		case 4:
    		
    			player[0] = 3;
    			player[1] = 1;
    			player[2] = 2;
    		
    			break;
    		
    		default :
    			
    			player[0] = 3;
    			player[1] = 2;
    			player[2] = 1;
    			
    	}
    }
    
    public int initiate()
    {
    	return player[2];
    }
	  
    public int next()
    {
    	if(data == 0)
    	{
    		data++;
    		
    		return player[2];	
    	}
    	
    	else if(data == 1)
    	{
    		data++;
    		
    		return player[0];	
    	}
    	
    	else if(data == 2)
    	{
    		data = 0;
    		
    		return player[1];	
    	}
    	
    	return 0;
    	
    }
}