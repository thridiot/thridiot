package thridiot;

import java.util.Queue;

public class Player
{
	private NodeArrayQueue piece;
	
	private int availablePass;
	
	private int playerNumber;
	
	private Node endNode;
	
	private Node head;
	
	private boolean alive;
	
	public Player(int playerNumber)
	{
		availablePass = 4;
		
		this.playerNumber = playerNumber;		
		
		alive = true;
		
		piece = new NodeArrayQueue(5);
	
	}
	
	public NodeArrayQueue getPiece()
	{
		return piece;
	}
	
	public int getAvailablePass()
	{
		return availablePass;
	}
	
	public int getPlayerNumber()
	{
		return playerNumber;
	}
	
	public void decresePass()
	{
		availablePass--;
	}
	
	public void setEndNode(Node endNode)
	{
		this.endNode = endNode;
	}
	
	public Node getEndNode()
	{
		return endNode;
	}
	
	public void setHead(Node head)
	{
		this.head = head;
	}
	
	public Node getHead()
	{
		return head;
	}
	
	public boolean isAlive()
	{
		return alive;
	}
	
	public void move(Node newHead)
	{
		piece.insert(newHead);
		
		head = newHead;
	}
	
	//true �¸�
	public boolean victory()
	{
		if(head.equals(endNode))
		{
			return true;
		}
		
		else
		{
			return false;
		}
		
		
	}
	
	//true �й�
	public boolean lose()
	{
		if(availablePass == 0)
		{
			alive = false;
			
			return true;
		}
		else
		{
			return false;
		}
		
		
	}

	public boolean isLocked()
	{
		if(		
				head.getLinkedNode(6).getData() != 0 &&
	    		head.getLinkedNode(1).getData() != 0 &&
	    		head.getLinkedNode(2).getData() != 0 &&
	    		head.getLinkedNode(3).getData() != 0 &&
	    		head.getLinkedNode(4).getData() != 0 &&
	    		head.getLinkedNode(5).getData() != 0
	      )
		{
			return true;
		}
		
		else
		{
			return false;
		}
	}
}
