	package thridiot;

import java.awt.Graphics;

import javax.swing.ImageIcon;

import javax.swing.JPanel;

public class Node extends JPanel
{
	static ImageIcon tileEmpty = new ImageIcon("tileEmpty.png"); 
	
	static ImageIcon tileBarricade = new ImageIcon("tileBarricade.png"); 
	
	static ImageIcon tilePlayer1Head = new ImageIcon("tilePlayer1Head.png"); 
	
	static ImageIcon tilePlayer1 = new ImageIcon("tilePlayer1.png"); 
	
	static ImageIcon tilePlayer2Head = new ImageIcon("tilePlayer2Head.png"); 
	
	static ImageIcon tilePlayer2 = new ImageIcon("tilePlayer2.png"); 
	
	static ImageIcon tilePlayer3Head = new ImageIcon("tilePlayer3Head.png"); 
	
	static ImageIcon tilePlayer3 = new ImageIcon("tilePlayer3.png"); 
	
	static ImageIcon tilePlayer1End = new ImageIcon("tilePlayer1End.png"); 
	
	static ImageIcon tilePlayer2End = new ImageIcon("tilePlayer2End.png"); 
	
	static ImageIcon tilePlayer3End = new ImageIcon("tilePlayer3End.png"); 
	
	
	
	public ImageIcon tile;
	
	final private int XVariation = tileEmpty.getIconWidth() / 2;
	
	final private int YVariation = tileEmpty.getIconHeight() * 3 / 4;
	
	private int orbit; //궤도
	
	private int order; //순서

	private int X;
	
	private int Y;
	
	//연결된 노드들 자신을 기준으로 0, 60, 180...에 있는 노드들
	private Node node0;
	private Node node60;
	private Node node120;
	private Node node180;
	private Node node240;
	private Node node300;
	
	// 0 = 없음, 1 = 플래이어 1, 2 = 플레이어 2, 3 = 플레이어 3,
	// 4 = 장애물, 5 = 플래이어 1 머리, 6 = 플래이어 2 머리, 7 = 플래이어 3 머리
	private int data;
	
	//검색할 때 쓰임 같은 
	//궤도안의 자기가 만들어 지기 직전 노드/자신이 만들어진 직후에 만들어진 노드
	private Node nodePrevious;
	private Node nodeNext;
	
	//궤도와 순서를 받아서 노드를 초기화
	public Node(int orbitInput, int orderInput)
	{
		this.orbit = orbitInput;
		
		this.order = orderInput;
		
		node0 = null;
		node60 = null;
		node120 = null;
		node180 = null;
		node240 = null;
		node300 = null;
		nodePrevious = null;
		nodeNext = null;
		
		data = 0;
		
		tile = new ImageIcon("tileEmpty.png"); 
	}
	
	//받은 값에 따라 노드를 리턴 1~6이면 방향
	//7,8은 전, 후 노드 서치에 쓰임
	public Node getLinkedNode(int input)
	{ 
		switch(input)
		{
		case 1:
			
			return node300;
			
		case 2:
			
			return node0;
			
		case 3:
	
			return node60;
	
		case 4:
	
			return node120;
	
		case 5:
	
			return node180;
	
		case 6:
	
			return node240;
			
		case 7:
		
			return nodePrevious;
			
		case 8:
			
			return nodeNext;
			
		default:
			
			return null;
			
		}
		
	}
	
	//방향(direction)과 다른 노드(inputNode)를 받아 이 노드와 연결 
	
	/*
	 숫자	연결(자신기준 inputNode의 위치 - inputNode기준 자신의 위치, {7, 8재외})
	 1	300-120
	 2	0-180
	 3	60-240
	 4	120-300
	 5	180-0
	 6	240-60
	 7	자신의 전 노드 = inputNode
	 8	자신의 다음 노드 = inputNode
	*/
	public void setLinkedNode(int direction, Node inputNode)
	{
		switch(direction)
		{
		case 1:
		
			this.node300 = inputNode;
			
			inputNode.node120 = this;
			
			break;
			
		case 2:
			
			this.node0 = inputNode;
			
			inputNode.node180 = this;
			
			break;
			
		case 3:
			
			this.node60 = inputNode;
			
			inputNode.node240 = this;
			
			break;
	
		case 4:
			
			this.node120 = inputNode;
			
			inputNode.node300 = this;
			
			break;
			
		case 5:	
			
			this.node180 = inputNode;
			
			inputNode.node0 = this;
			
			break;
	
		case 6:	
			
			this.node240 = inputNode;
			
			inputNode.node60 = this;
			
			break;
			
		case 7:	
			
			this.nodePrevious = inputNode;
			
			inputNode.nodeNext = this;
			
			break;
			
		case 8:	
	
			this.nodeNext = inputNode;
	
			inputNode.nodePrevious = this;
	
			break;
			
			
		}
		
	}
	
	//노드의 궤도값을 리턴
	public int getOrbit()
	{
		return orbit;
	}
	
	//노드의 순서를 리턴
	public int getOrder()
	{
		return order;
	}
	
	public void setData(int input)
	{
		this.data = input;
	}
	
	public int getData()
	{
		return data;
	}

	public int getHeight()
	{
		return tileEmpty.getIconHeight();
	}
	
	public int getWidth()
	{
		return tileEmpty.getIconWidth();
	}
	
	public void paintComponent(Graphics g) 
	{
		g.drawImage(tile.getImage(), 0, 0, null);
		
		setOpaque(false);
		
		super.paintComponent(g);
	}
	
	public int getX()
	{
		return X;
	}
	
	public void setX(int input)
	{
		this.X = input;
	}
	
	public int getY()
	{
		return Y;
	}
	
	public void setY(int input)
	{
		this.Y = input;
	}

	public void setLocation(int direction, Node previous)
	{
		switch(direction)
		{
		case 1 :
			
			this.X = previous.X - previous.XVariation;
			
			this.Y = previous.Y - previous.YVariation;
			
			break;
			
		case 2 :
			
			this.X = previous.X - previous.XVariation * 2;
			
			this.Y = previous.Y;
			
			break;
			
		case 3 :
	
			this.X = previous.X - previous.XVariation;
			
			this.Y = previous.Y + previous.YVariation;
			
			break;
	
		case 4 :
	
			this.X = previous.X + previous.XVariation;
			
			this.Y = previous.Y + previous.YVariation;
			
			break;
	
		case 5 :

			this.X = previous.X + previous.XVariation * 2;
			
			this.Y = previous.Y;
			
			break;
			
		case 6 :
			
			this.X = previous.X + previous.XVariation;
			
			this.Y = previous.Y - previous.YVariation;
			
			break;
		}
	}

	// 0 = 없음, 1 = 플래이어 1, 2 = 플레이어 2, 3 = 플레이어 3,
	// 4 = 장애물, 5 = 플래이어 1 머리, 6 = 플래이어 2 머리, 7 = 플래이어 3 머리, 8 = 1끝, 9 = 2끝, 10 = 3끝,
	public void setStatus(int input)
	{
		switch(input)
		{
		case 0:
			
			tile = tileEmpty;
			
			data = input;
			
			break;
			
		case 1:
			
			tile = tilePlayer1;
			
			data = input;
			
			break;
			
		case 2:
	
			tile = tilePlayer2;
			
			data = input;
			
			break;
	
		case 3:
			
			tile = tilePlayer3;
			
			data = input;
			
			break;
	
		case 4:
			
			tile = tileBarricade;
			
			data = input;
			
			break;
	
		case 5:
			
			tile = tilePlayer1Head;
			
			data = input;
			
			break;
	
		case 6:
			
			tile = tilePlayer2Head;
			
			data = input;
			
			break;
	
		case 7:
			
			tile = tilePlayer3Head;
			
			data = input;
			
			break;
		
		case 8:
			
			tile = tilePlayer1End;
			
			data = 0;
			
			break;
			
		case 9:
	
			tile = tilePlayer2End;
	
			data = 0;
	
			break;
	
		case 10:
	
			tile = tilePlayer3End;
	
			data = 0;
	
			break;
		
		}
	}
}
