package thridiot;

public class NodeArrayQueue 
{
    private int tail;
    
    private int maxSize;
    
    private Node[] queueArray;
    
    public  NodeArrayQueue(int maxSize)
    {
        this.tail = -1;
        
        this.maxSize = maxSize;
        
        this.queueArray = new Node[maxSize];
    }
    
    public boolean isEmpty()
    {
        return (0 == tail+1);
    }
    
    public boolean isFull()
    {
        return (tail == maxSize-1);
    }
    
    public void insert(Node input)
    {
        
        if(isFull())
        {
        	for(int i = 0; i < maxSize - 1; i++)
        	{
        		queueArray[i] = queueArray[i + 1];
        	}
        	
        	queueArray[maxSize - 1] = input;
        }
        else
        {
        	queueArray[++tail] = input;
        }
        
        for(int i = 0; i <= tail; i++)
        {
        	System.out.print("(" + queueArray[i].getOrbit() + ", " + queueArray[i].getOrder() + ")");
        }
        
        System.out.println();
        //System.out.print("(" + queueArray[0].getOrbit() + ", " + queueArray[0].getOrder() + ")");
    }
    
    public Node peek()
    {
        
        if(isEmpty())
        {
        	throw new ArrayIndexOutOfBoundsException();
        }
        
        return queueArray[0];
    }
    
    public  Node remove(){
        
        Node item = peek();
        
        for(int i = 0; i < tail; i++)
    	{
    		queueArray[i] = queueArray[i + 1];
    	}
        
        queueArray[tail] = null;
        
        tail--;
        
        return item;
    }
    
    public int getTail()
    {
    	return tail;
    }

}