package kim;

import java.util.Scanner;

//장애물 좌표 저장
class ObtacleSave {
	int X, Y;
	public ObtacleSave(int X, int Y) {
		this.X = X;
		this.Y = Y;
	}
}

public class Location 
{
	public int X;
	
	public int Y;
	
	//장애물 수 입력
	Scanner sc = new Scanner(System.in);
	int num = sc.nextInt();
	
	//장애물 저장할 배열 생성
	ObtacleSave [] OS = new ObtacleSave[num];
	
	//장애물 좌표
	public void Random (int X, int Y){
		for(int i=0;i<num;i++){
			X = (int)(Math.random()*6 + 1);
			Y = (int)(Math.random()*(X*6-1) + 1);
			
			//시작점과 도착점 앞 위치 제외
			if(X == 6){
				if(Y == 0 || Y == 6 || Y == 12 || Y == 18 || Y == 24 || Y == 30){
					X = (int)(Math.random()*6 + 1);
					Y = (int)(Math.random()*(X*6-1));
				}
			}
			
			//배열에 저장
			OS[i] = new ObtacleSave(X,Y);
			
			//중복체크
			for(int j=0;j<i;j++){
				if (OS[i] == OS[j]) {
					X = (int)(Math.random()*6 + 1);
					Y = (int)(Math.random()*(X*6-1));
					
					OS[i] = new ObtacleSave(X,Y);
					
					i = i-1;
					
					break;
				}
			}
		}
		
	}
}